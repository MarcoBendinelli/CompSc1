`timescale 1ns / 1ps

module uncompleteSpec(a,b,c,sel,out,out_neg);

input a,b,c;
input [1:0] sel;
output reg out;
output reg out_neg;
	 
	
	always @(a,b,c) //always @(*) incomplete sensitivity list
	begin
		//out = a;
		case (sel)
		2'b00: out =a;
		2'b01: out =b;
		2'b10: out =c;
		// complete case!!
		//default case statement for X
		endcase
		out_neg=~out;
	end
endmodule
