`timescale 1ns / 1ps

module testBenchUncompleteSpec();
	reg clk;
	reg a, b, c;
	reg [1:0] sel;
	wire out, out_neg;

//	this shows an additional inferred latch due to design errors
	uncompleteSpec dut(.a(a), .b(b), .c(c), .sel(sel), .out(out), .out_neg(out_neg));
	initial
	begin
	
		a=0;b=0;c=0;sel=0;clk=0;
		$monitor ("Time = %0d clk=%d f(A-%d,B-%d,S-%d)->(%d,%d))",$time, clk,a,b,sel,out,out_neg);	
		#5; a=1;b=0;sel=0;
		#7; a=1;b=0;sel=1;
		#10; a=1;b=1;sel=0;
		#15; a=1;b=0;sel=1;
		#20; a=1;b=0;sel=0;
		#5; a=1;b=0;sel=3;
		
		// something subtle about synthesis and verification!!!! The X value, don't care for synthesis and unknown in simulation
		#5; a=1'bx;b=0;sel=1;
		#5; a=1;b=0;sel=1'bx;
		#20;
		$finish;

		$finish;
	end
	
	always 
	begin
		#5 clk=~clk;
	end

endmodule
