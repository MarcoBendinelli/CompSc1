`timescale 1ns / 1ps

module testbenchArbSeqPrio();
reg clk,rst,a,b,c,d;
wire [3:0] g;
	
	arbSeqPrio arbSeqPrio(.clk(clk), .rst(rst), .a(a), .b(b), .c(c), .d(d), .g(g));
	
	initial
	begin
		$monitor("clk%d rst=%d %d%d%d%d->%4b",clk,rst,a,b,c,d,g);
		
		clk=0;rst=1;
		a=0;b=0;c=0;d=0;
		repeat(2) @(posedge clk);
		rst=0;
		repeat(2) @(posedge clk);
		a=1;d=1;
		repeat(2) @(posedge clk);
		#2 a=0;
		repeat(2) @(posedge clk);
		#2 b=1;
		repeat(2) @(posedge clk);
		#2 a=1'bx;b=1;c=0;d=0;
		repeat(2) @(posedge clk);
		#2 a=1'bx;b=0;c=0;d=0;
		repeat(2) @(posedge clk);
		#2 a=1;b=1'bx;
		repeat(2) @(posedge clk);
	end


	always #5 clk=~clk;
endmodule
