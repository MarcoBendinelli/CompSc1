% Generating a (pure) Barabasi-Albert scale-free undirected network

clear all

% randseed=fix(clock)*[0 3 5 7 11 13]';
% rand('state',randseed);
rand('state',0);

N=10000;     %total number of nodes
m=3;        %initial number of nodes (and minimal node connectivity)

A=sparse(N,N);      %connectivity matrix
deg=zeros(1,N);     %node degree vector

%initial connectivity matrix: full connection of (m+1) nodes
A(1:m+1,1:m+1)=ones(m+1)-eye(m+1);
deg(1:m+1)=m;
degtot=sum(deg(1:m+1));

%growing the network up to N nodes
disp(['Growing BA scale-free network'])
for j=m+2:N
    
    if rem(j,100)==0
        disp(['Adding node ',int2str(j),' of ',int2str(N)])
    end;
    
    %adding node j
    %selecting the m links to be added
    for i=1:m
        okay=0;
        while okay==0
            rn=degtot*rand;
            r=1;
            partial=deg(1);
            while rn>partial
                r=r+1;
                partial=partial+deg(r);
            end;
            %r is the i-th candidate node to be connected with j
            if A(j,r)==0
                okay=1;
                A(j,r)=1;
            end;
        end;
    end;
    %copying the new j-th line into the new j-th column
    A(1:j-1,j)=A(j,1:j-1)';
    ind(1:j-1)=(A(1:j-1,j)>0);
    deg(ind)=deg(ind)+1;
    deg(j)=m;
    degtot=degtot+2*m;
end;
        
%final degree vector
avgdeg=full(mean(deg));
maxdeg=full(max(deg));
mindeg=full(min(deg));
disp(['Average degree = ',num2str(avgdeg),' (Expected = ',num2str(2*m),')'])
disp(['Max degree = ',num2str(maxdeg),'    Min degree = ',num2str(mindeg)])

%degree distribution
degrees=mindeg:maxdeg;
h=hist(deg,maxdeg-mindeg+1);     
h=h/sum(h);
%cumulative degree distribution
for j=1:length(h)
    hcum(j)=sum(h(j:length(h)));
end;
warning off
temp=[log(degrees)' log(hcum)'];
temp(any(isinf(temp)'),:) = [];
ptot=polyfit(temp(:,1),temp(:,2),1);
disp(['Power-law exponent = ',num2str(ptot(1)-1),' (Expected = ',num2str(-3),')'])
pini=polyfit(log(degrees(1:floor(maxdeg/10))),log(hcum(1:floor(maxdeg/10))),1);
disp(['Initial power-law exponent = ',num2str(pini(1)-1),' (Expected = ',num2str(-3),')'])
plot(log(degrees),log(hcum),'.',log(degrees),polyval(ptot,log(degrees)),log(degrees),polyval(pini,log(degrees)))
% plot(log(degrees),log(h),'.',log(degrees),polyval(ptot,log(degrees)),log(degrees),polyval(pini,log(degrees)))
% plot(degrees,h,,'.',degrees,exp(polyval(ptot,log(degrees))),degrees,exp(polyval(pini,log(degrees))))
warning on

%saving the connectivity matrix
% save NetBAlogn2m3.mat A N deg
% spy(A)
