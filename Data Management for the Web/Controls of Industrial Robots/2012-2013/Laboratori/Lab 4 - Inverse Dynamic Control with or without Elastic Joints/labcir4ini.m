r=input('Do you want to simulate elastic joints? (y/n) ','s');

%### Passo 1 ###

%Scrivere i parametri del robot

%Moments of inertia of the motors
Jm1=5.e-3;
Jm2=2.e-3;

%Reduction ratios
n1=100;
n2=100;
N=diag([n1,n2]);

%Robot definition

L1=link([0,1,0,0,0]);
L1.m=50;
L1.r=[-0.5,0,0];
L1.I=[0,0,0;0,0,0;0,0,10];
L1.G=n1;
L1.Jm=Jm1;

L2=L1;
L2.G=n2;
L2.Jm=Jm2;

r2=robot({L1,L2});

r2.name='POLIrobot';
r2.gravity=[0,9.81,0];


%Initial instant of motion
t0=0.1;

%Travel time
T=0.5;

%Maximum speed
sd=5;

%Distance to cover
h=1.6;

%Acceleration time
ta=(T*sd-h)/sd;

%Maximum acceleration
amax=sd/ta;

%Initial value for the trajectory
s0=0.2;

%Initial joint positions
q0=ikine(r2,[eye(3),[0.2;0;0];[0,0,0,1]],[-1,3],[1 1 0 0 0 0 ]);


%### Passo 2 ###

%Compose in Simulink the diagram that performs the computation of the 
%inverse dynamics. The block takes as inputs joint positions and velocities
% as well as a third vector, that will be formed by the outputs of PD 
% controllers. This vector plays the role of acceleration vector in the 
%computation of the inverse dynamics. You will use the Newton-Euler 
%algorithm (rne) available in the Toolbox. The Matlab function to be 
%indicated in the related block will then be:

%rne(r2, u(1:2)', u(3:4)', u(5:6)')

%where r2 is the name of the previously defined robot object

%%%Explanation%%%

%INVERSE DYNAMIC MODEL

%? = B(q)??q + n(q,?q)
%n(q,?q) = C(q,?q)*?q + g(q)

% Schema con un BLOCCO RNE e in input q,?q,??q e in output ?

%INVERSE DINAMIC MODEL FOR CONTROL
%? = B(q) * v + n(q,?q)
%quello che cambia � che v il nuovo control input 
%� al posto di ??q per calcolare ?.

% Schema con un BLOCCO RNE e in input q,?q,v e in output ?
% La cosa � pi� facile di quello che pensiamo, perch� effettivamente
% Il robot si muove allo stesso modo solo che le accelerazioni sono
% Calcolate in modo robusto

%### Passo 3 ###

%Compose now the diagram that implements the PD controllers. 
%It takes as inputs joint positions and velocities, both actual and desired
% ones, computes the related errors and multiplies them by the gains KP and
% KD (identical for the two joints).

%### Passo 4 ###

%Using the blocks prepared in the previous lab sessions, assemble the 
%Simulink model reported in the picture.

%For both the joints compute the gains KP and KD in such a way to assign 
%to the closed loop poles of the system, made linear and decoupled by 
%the application of the inverse dynamics method, a natural frequency of 
%100 rad/s and a damping factor of 0.7.

%How to tune:

%SE IL MODELLO � PERFETTO ALLORA:

%v = ??q

%Quindi un buon modo per controllarlo � usare un PD in questa forma:

%v = Kp(q_d-q) + Kd(?q_d-?q)+??q_d

%Per ottenere:

%??q = Kp(q_d-q) + Kd(?q_d-?q)+??q_d

%e definendo ~q = q_d - q otteniamo:

%??~q + Kd*?~q + Kp*~q = 0 %%DINAMICA DELL'ERRORE

%Tale dinamica � governata da una legge del tipo:

%s^2 + Kd*s + Kp = 0

%Se vogliamo w_n = 100 rad/s e ? = 0.7 quando dovranno essere Kp e Kd?

%Basta fare Kd = 2*?*w_n e Kp = w_n^2 quindi:

%Kp = 10000, Kd = 140

%Natural frequency and damping coefficients of the nominal closed loop
%poles
wn=100;
csi=0.7;

%Proportional and derivative gains
KP=wn^2;
KD=2*csi*wn;


%### Passo 5 ###

%Simulate the system, taking care of indicating in the mask of the block 
%that simulates the robot, within the ?Robot object? field, the string 
%perturb(r2), which modifies the dynamic parameters of the robot by 10% 
%with respect to the nominal values. This is to provide a more realistic 
%simulation scenario.


%### Passo 6 ###

%Assume now that the joints of the manipulator are affected by 
%torsional flexibility. 

%Adopt the following numerical values:
%-stiffness constants of the transmissions: Kel1= Kel2= 70 Nm/rad
%-viscous friction coefficients of the transmissions Del1= Del2= 0.05 Nms/rad

%Set to zero the moments of inertia of the motors inside the model of the 
%robot, and compose, using the blocks prepared in the previous lab 
%sessions, the following Simulink diagram.

%Keeping the same gains of the PD controllers previously determined, 
%simulate the system and compare the performance to the case of 
%manipulator with rigid joints.

%Case of elastic joints

if r=='y'
 %Robot redefinition
  L1.Jm=0;
  L2.Jm=0;
  r2=robot({L1,L2});
  r2.name='POLIrobot';
  r2.gravity=[0,9.81,0];

  %Stiffness constants of the transmissions
  Kel1=70;
  Kel2=70;
  Kel=diag([Kel1,Kel2]);

  %Viscous friction coefficients of the transmissions
  Del1=0.05;
  Del2=0.05;
  
  %Initial reference position at the joints
  qrif0=ikine(r2,[eye(3),[0.2;0;0];[0,0,0,1]],[-1,3],[1 1 0 0 0 0 ]);

  %Initial motor positions
  qm0=qrif0*N;

  %Initial link positions
  q0=fsolve(@(q0) N*Kel*(qm0'-N*q0')-gravload(r2,q0)',qrif0);

end


%### Passo 6 ###

%Try out also the "PD plus gravity compensation" solution


%tau =  Kp(q_d-q)   -  Kd*?q   + g(q)

%           ^            ^        ^
%           |            |        |
%           |            |        |

%      Proportional Derivative Gravity

% possiamo anche calcolare Kp e Kd segnati dall'inverse dynamics

% Poi possiamo calcolare Kp = B*Kp entrambi segnati e Kd = B * Kd

%dove B segnato � una diagonally constant approximation of the intertia
%matrix
