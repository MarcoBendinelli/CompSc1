
#ifndef ACTIVE_OBJECT_H_
#define ACTIVE_OBJECT_H_

#include <thread>
#include <atomic>

class ActiveObject
{
public:
	ActiveObject();
	
	~ActiveObject();
private:
	
	virtual void run();
	
	ActiveObject(const ActiveObject&);
	ActiveObject& operator=(const ActiveObject&);
	
protected:
	std::thread t;
	std::atomic<bool> quit;
};

#endif //ACTIVE_OBJECT_H_
