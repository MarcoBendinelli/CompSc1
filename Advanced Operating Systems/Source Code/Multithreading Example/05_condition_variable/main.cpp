
#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>

using namespace std;

string shared;
mutex myMutex;
condition_variable myCv;

void myThread()
{
	unique_lock<mutex> lck(myMutex);
	if(shared.empty()) myCv.wait(lck);
	cout<<shared<<endl;
}

int main()
{
	thread t(myThread);
	string s;
	cin>>s; //Read from stdin
	{
		unique_lock<mutex> lck(myMutex);
		shared=s;
		myCv.notify_one();
	}
	t.join();
}