
#include <iostream>
#include <thread>

using namespace std;

void myThread(int i, const string& s)
{
	cout<<"Called with "<<i<<" and "<<s<<endl;
}

int main()
{
	thread t(myThread,2,"test");
	t.join();
}
