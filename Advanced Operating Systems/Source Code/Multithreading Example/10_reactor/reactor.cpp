
#include "reactor.h"

using namespace std;

void doNothing() {}

void Reactor::pushTask(function<void ()> func)
{
	queue.put(func);
}

Reactor::~Reactor()
{
	quit.store(true);
	pushTask(&doNothing);
	t.join();  //Thread derived from ActiveObject
}

void Reactor::run()
{
	while(!quit.load()) queue.get()();  //Get a function and call it
}
