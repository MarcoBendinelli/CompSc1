
#include "registers.h"
#include "serial.h"

//PD14: red led

void delay()
{
	volatile int i;
	for(i=0;i<1000000;i++) ;
}

void blink()
{
	for(int i=0;i<10;i++)
	{
		GPIOD->BSRR=1<<14;
		delay();
		GPIOD->BSRR=1<<(14+16);
		delay();
	}
}

SerialPort serial;

int main()
{
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;
	GPIOD->MODER |= 1<<28;
	bool led=false;
	for(;;)
	{
		char c=serial.read();
		if(c=='+') blink();
		else {
			char str[24]="Received chatacter:  \r\n";
			str[20]=c;
			serial.write(str);
		}
	}
}
