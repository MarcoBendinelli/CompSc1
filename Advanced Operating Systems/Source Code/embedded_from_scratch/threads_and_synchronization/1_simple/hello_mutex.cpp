
#include <stdio.h>
#include <pthread.h>

pthread_mutex_t mutex=PTHREAD_MUTEX_INITIALIZER;
int counter=0;

void increment()
{
    pthread_mutex_lock(&mutex);
    counter++;
    pthread_mutex_unlock(&mutex);
}

void *threadFunction(void *arg)
{
    for(int i=0;i<50;i++) increment();
}

int main()
{
    pthread_t t;
    pthread_create(&t,NULL,&threadFunction,NULL);
    for(int i=0;i<50;i++) increment();
    pthread_join(t,NULL);
    printf("Final value %d\n",counter);
}
