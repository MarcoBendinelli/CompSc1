
#include <pthread.h>
#include <miosix.h>
#include <unistd.h>

using namespace miosix;

typedef Gpio<GPIOD_BASE,14> led1;
typedef Gpio<GPIOD_BASE,15> led2;

void *threadFunction(void *arg)
{
    for(;;)
    {
        led2::high();
        usleep(0.5/2*1000000);  //Blink led2 @ 2Hz
        led2::low();
        usleep(0.5/2*1000000);
    }
}

int main()
{
    led1::mode(Mode::OUTPUT);
    led2::mode(Mode::OUTPUT);

    pthread_t t;
    pthread_create(&t,NULL,&threadFunction,NULL);
    for(;;)
    {
        led1::high();
        usleep(0.333/2*1000000);  //Blink led2 @ 3Hz
        led1::low();
        usleep(0.333/2*1000000);
    }
}
