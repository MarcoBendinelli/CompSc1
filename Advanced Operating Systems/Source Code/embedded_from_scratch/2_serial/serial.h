
#ifndef SERIAL_H
#define SERIAL_H

class SerialPort
{
public:
	SerialPort();

	void write(const char *s);
};

#endif //SERIAL_H
